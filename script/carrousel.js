//Caroussel 
//Définition du carrousel
//Création index et mise a 0
let i = 0;
//Récupérer l'image 1 et caché les autres
  $("section:nth-of-type(3)>div>img" ).slice(1).hide();
  $("section:nth-of-type(3)>article>p" ).slice(1).hide();
//Ecouter le clique sur le bouton de droite
$("section:nth-of-type(3)>img:nth-of-type(2)").click(() => {
  //Si l'index est supérieur au nombre d'image le remettre à 0
  if(i>10) {
    i = 0;
  }
  //Caché l'image actuelle et afficher la suivante 
  $("section:nth-of-type(3)>div>img" ).eq(i+1).show().end().eq(i).hide();
  //Cahé le nom du personnage actuelle et afficher le suivant
  $("section:nth-of-type(3)>article>p" ).eq(i+1).show().end().eq(i).hide();
  console.log(i)
  //Incrémenter de 1 l'Index
  i = i + 1;
});

//Ecouter le clique sur le bouton de gauche
$("section:nth-of-type(3)>img:nth-of-type(1)").click(() => {
  //Si l'index est inférieur au nombre d'image t le remettre à 10
  if(i<0) {
    i = 10 ;
  }
  //Caché l'image actuelle et afficher la  précédente 
  $("section:nth-of-type(3)>div>img" ).eq(i-1).show().end().eq(i).hide();
    //Cahé le nom du personnage actuelle et afficher le précédent
  $("section:nth-of-type(3)>article>p" ).eq(i-1).show().end().eq(i).hide();
    //Retirer 1 à l'Index
  i = i - 1;
});

