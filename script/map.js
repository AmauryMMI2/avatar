//Création de la map
var map = L.map('map').setView([51.311, 5.068], 13);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 14,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

//Ajout des points sur la carte
var layerGroup = L.geoJSON(cinema, {
    onEachFeature: function (feature, layer) {
      //Choix des différentes propriété à afficher
      layer.bindPopup('<p>'+feature.properties.NOM_ETABLISSEMENT+'</p><p>Situé a '+feature.properties.COMMUNE+', '+feature.properties.CODE_COMMUNE+'</p><p>'+feature.properties.ECRANS+' salles</p>');
    },
  }).addTo(map);

  
// var c = b - a;
// var e = Math.sin(a)*Math.sin(b)+Math.cos(a)*Math.cos(b)*Math.cos(c);
// var d = Math.arcos(e);
//Coordonnées Comprise entre long, lat et long+1, lat+1

//Récupération de coordonée de l'utilisateur
let creaCercle = true
// Appelée si récupération des coordonnées réussie
function positionSucces(position) {
    // Injection du résultat dans du texte
    const lat = Math.round(1000 * position.coords.latitude) / 1000;
    const long = Math.round(1000 * position.coords.longitude) / 1000;
    $("section:nth-of-type(5)>article>p").text(`Latitude: ${lat}°, Longitude: ${long}°`);
    map.flyTo([lat, long], 11)
    //Ajout d'un cercle sur la carte
    if(creaCercle==true){
    var circle = L.circle([lat, long], {
        color: 'rgba(28, 29, 114, 0.527)',
        fillColor: 'transparent',
        fillOpacity: 0.2,
        radius: 6000
    }).addTo(map);
     creaCercle = false
}

  }
  
  // Appelée si échec de récuparation des coordonnées
  function positionErreur(erreurPosition) {
    // Cas d'usage du switch !
    let natureErreur;
    switch (erreurPosition.code) {
      case erreurPosition.TIMEOUT:
        // Attention, durée par défaut de récupération des coordonnées infini
        natureErreur = "La géolocalisation prends trop de temps...";
        break;
      case erreurPosition.PERMISSION_DENIED:
        natureErreur = "Vous n'avez pas autorisé la géolocalisation.";
        break;
      case erreurPosition.POSITION_UNAVAILABLE:
        natureErreur = "V   otre position n'a pu être déterminée.";
        break;
      default:
        natureErreur = "Une erreur inattendue s'est produite.";
    }
    // Injection du texte
    $("section:nth-of-type(5)>article>p").text(natureErreur);
  }
  
  // Récupération des coordonnées au clic sur le bouton
  $("section:nth-of-type(5)>article>button").click(function () {
    // Support de la géolocalisation
    if ("geolocation" in navigator) {
      // Support = exécution du callback selon le résultat
      navigator.geolocation.getCurrentPosition(positionSucces, positionErreur, {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 30000,
      });
    
    } else {
      // Non support = injection de texte
      $("section:nth-of-type(5)>article>p").text("La géolocalisation n'est pas supportée par votre navigateur");
    }
  });
  

//Geojson
// retourner filter en false, comment utiliser le filter