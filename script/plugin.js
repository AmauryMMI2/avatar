//Plugin  de Kaluzny Adrien
//création de du plugin
$.fn.parallax = function (options) {
  const htmlElement = $(this);
    //Ecoute du scroll sur la page
    $(window).scroll(function() {
      const 
      //création d'une variable a qui récupère la position de scroll
        a = $(window).scrollTop(),
        //Récupération du paramétre de vistesse du scroll
        b = options;
        //Modification du css pour $(this) -> à définir dans $(this).parallax(nombre)
      $('section').css({
        //Modification de la background position x en fonction de scroll
        backgroundPositionX: a / b + "px",
        // backgroundSize: cover
      });
    // Mise en place du parallax
    $('section').each(function () {
      const 
      //Récupération de la position dans la page
        top = window.pageYOffset,
        //Récupération du $this (inutile)
        one = htmlElement;
    //Mise en place du style qui va bouger le background de 1px par rapport au top
      one.css.right = -(top + 1) + "px";
      $('section').css({
        backgroundSize:100 + a / b +"vw",
        backgroundPositionX: "left"
      });
    }
  )}
 );
    return this;
  };

